<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='copyright' content=''>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Title Tag  -->
    <title>Eshop - eCommerce HTML5 Template.</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{asset('theme/images/favicon.png')}}">
    <!-- Web Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

    <!-- StyleSheet -->

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/css/bootstrap.css')}}">
    <!-- Magnific Popup -->
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/magnific-popup.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/font-awesome.css')}}">
    <!-- Fancybox -->
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/jquery.fancybox.min.css')}}">
    <!-- Themify Icons -->
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/themify-icons.css')}}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/niceselect.css')}}">
    <!-- Animate CSS -->
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/animate.css')}}">
    <!-- Flex Slider CSS -->
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/flex-slider.min.css')}}">
    <!-- Owl Carousel -->
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/owl-carousel.css')}}">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/slicknav.min.css')}}">

    <!-- Eshop StyleSheet -->
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/reset.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/style.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('theme/css/responsive.css')}}">

@yield('links')
    <style type="text/css">

    </style>
</head>
