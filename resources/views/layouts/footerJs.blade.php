<script src="{{asset('theme/js/jquery.min.js')}}"></script>
<script src="{{asset('theme/js/jquery-migrate-3.0.0.js')}}"></script>
<script src="{{asset('theme/js/jquery-ui.min.js')}}"></script>
<!-- Popper JS -->
<script src="{{asset('theme/js/popper.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{asset('theme/js/bootstrap.min.js')}}"></script>
<!-- Color JS -->
<script src="{{asset('theme/js/colors.js')}}"></script>
<!-- Slicknav JS -->
<script src="{{asset('theme/js/slicknav.min.js')}}"></script>
<!-- Owl Carousel JS -->
<script src="{{asset('theme/js/owl-carousel.js')}}"></script>
<!-- Magnific Popup JS -->
<script src="{{asset('theme/js/magnific-popup.js')}}"></script>
<!-- Waypoints JS -->
<script src="{{asset('theme/js/waypoints.min.js')}}"></script>
<!-- Countdown JS -->
<script src="{{asset('theme/js/finalcountdown.min.js')}}"></script>
<!-- Nice Select JS -->
<script src="{{asset('theme/js/nicesellect.js')}}"></script>
<!-- Flex Slider JS -->
<script src="{{asset('theme/js/flex-slider.js')}}"></script>
<!-- ScrollUp JS -->
<script src="{{asset('theme/js/scrollup.js')}}"></script>
<!-- Onepage Nav JS -->
<script src="{{asset('theme/js/onepage-nav.min.js')}}"></script>
<!-- Easing JS -->
<script src="{{asset('theme/js/easing.js')}}"></script>
<!-- Active JS -->
<script src="{{asset('theme/js/active.js')}}"></script>
@yield('scripts')
