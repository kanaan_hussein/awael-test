@extends('layouts.master')
@section('metaDesc')
@endsection

@section('links')
    <style>

    </style>
@endsection
{{--@section('pageName') @lang('pagesname.website') @endsection--}}

@section('body')
    <div class="product-area section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2>Product Details</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="product-info">
                        <div class="tab-content" id="myTabContent">
                            <!-- Start Single Tab -->

                            <div class="tab-single" >
                                <div class="row">
                                            <div class="single-product">
                                                <div class="section-title">
                                                    <h2>{{$product->name}}</h2>
                                                </div>
                                                <span>{{$product->manufacturedCountry}}</span>
                                                <div class="product-img">
                                                    <a href="product-details.html">
                                                        <img class="default-img" src="{{$product->img[0]}}" alt="#">
                                                        <img class="hover-img" src="{{$product->img[0]}}" alt="#">
                                                    </a>
                                                    <div class="button-head">
                                                        <div class="product-action">
                                                            <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#"><i class=" ti-eye"></i><span>Quick Shop</span></a>
                                                            <a title="Wishlist" href="#"><i class=" ti-heart "></i><span>Add to Wishlist</span></a>
                                                            <a title="Compare" href="#"><i class="ti-bar-chart-alt"></i><span>Add to Compare</span></a>
                                                        </div>
                                                        <div class="product-action-2">
                                                            <a title="Add to cart" href="#">Add to cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <h3><a href="product-details.html">{{$product->description}}</a></h3>
                                                    <div class="product-price">
                                                        <span>{{$product->color}}</span>
                                                    </div>
                                                    <div class="product-price">
                                                        <span>{{$product->guestPrice}}$</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <!--/ End Single Tab -->

                    </div>
                </div>
            </div></div>


@endsection
@section('scripts')
    <script>
    </script>
@endsection
