<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => array('product')]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function product()
    {
        $products = Product::all();
        $products->each(function ($products, $key) {
            $temp = json_decode($products->img);
            for ($i = 0; $i < count($temp); $i++) {
                $temp[$i] = asset('storage') . '/' . $temp[$i];
            }
            $products->img = $temp;
            unset($products->updated_at);
        });
        return view('welcome', compact('products'));
    }


    public function index()
    {
        $products = Product::all();
        $products->each(function ($products, $key) {
            $temp = json_decode($products->img);
            for ($i = 0; $i < count($temp); $i++) {
                $temp[$i] = asset('storage') . '/' . $temp[$i];
            }
            $products->img = $temp;
            unset($products->updated_at);
        });
        return view('home', compact('products'));


    }
}
