<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('brand_id');

            $table->foreign('brand_id')->references('id')->on('brands');
            $table->unsignedBigInteger('category_id');

            $table->foreign('category_id')->references('id')->on('categories');
            $table->string('name');
            $table->string('img');
            $table->string('description');
            $table->float('guestPrice');
            $table->float('creatorPrice');
            $table->boolean('hasOffer')->nullable();
            $table->float('offerPrice')->nullable();
            $table->integer('quantity');
            $table->string('manufacturedCountry')->nullable();
            $table->string('color');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
